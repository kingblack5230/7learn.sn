<?php
namespace App\Services\Router;

use App\Core\Request;
use App\Services\View\View;

class Router{
    public static $routes;
    const baseController = "\App\Controller\\";
    const baseMiddleware = "\App\Middleware\\";

    public static function start()
    {
//        echo "Router Started !";
        // get all routes
        self::$routes = include BASE_PATH."routes/web.php" ;
        // get current route
        $currentRoute = self::getCurrentRoute();

        // check if current route exists
        if(self::routeExists($currentRoute)){
            // is allowed Method ?
            if(!in_array(strtolower($_SERVER['REQUEST_METHOD']),self::getRouteMethods($currentRoute))){
                header('HTTP/1.0 403 Forbidden');
                View::load("errors.403");
                die();
            }

            $request = new Request();
            // middleware ...
            $middlewareClass = self::baseMiddleware .self::getRouteMiddleware($currentRoute)."Middleware";
            if(class_exists($middlewareClass)){
                $middlewareInstance = new $middlewareClass;
                $middlewareInstance->handle($request);
            }else{
                echo "MiddleWare not exists !";
                die();
            }

            // get current target
            $target = self::getRouteTarget($currentRoute);
            list($controllerClass,$method) = explode('@',$target); // [controller,method]
            $fullControllerClass = self::baseController . $controllerClass;
            if(!class_exists($fullControllerClass)){
                echo "Class not exists !";
                die();
            }
            $controller = new $fullControllerClass;
            if(method_exists($controller,$method)){
                // call method from controller

                $controller->$method($request);
            }else{
                echo "Method not exists !";
                die();
            }


        }else{
            header("HTTP/1.0 404 Not Found");
            View::load("errors.404");
        }
    }

    public static function getCurrentRoute()
    {
        return strtok(strtolower($_SERVER['REQUEST_URI']),'?');
    }
    public static function routeExists($route)
    {
        return array_key_exists($route,self::$routes);
    }
    public static function getRouteTarget($route)
    {
        return self::$routes[$route]['target'];
    }
    public static function getRouteMiddleware($route)
    {
        return self::$routes[$route]['middleware'] ?? null;
    }
    public static function getRouteMethods($route)
    {
        return explode('|',self::$routes[$route]['method']);
    }




}