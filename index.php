<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

require_once "bootstrap/Constants.php";
require_once "vendor/autoload.php";
require_once "bootstrap/init.php";
require_once "config/parameters.php";

\App\Services\Router\Router::start();