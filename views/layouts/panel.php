<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>7Learn</title>
    <style>
        .header, .footer {
            height: 40px;
            line-height: 40px;
            padding: 10px;
            text-align: center;
        }

        .header {
            background: #dcf5ff;
            font-size: 20px;
        }

        .footer {
            background: #ddffd6;
        }

        .content {
            float: left;
            width: 70%;
        }

        .side {
            float: right;
            width: 29%;
            background: antiquewhite;
        }
        .clear{
            clear: both;
        }
    </style>
</head>
<body>
<div class="header">Header</div>
<div>
    <div class="content">
        <?= $renderedView; ?>
    </div>
    <div class="side">
        Sidebar
    </div>
    <div class="clear"></div>
</div>
<div class="footer">Footer</div>
</body>
</html>